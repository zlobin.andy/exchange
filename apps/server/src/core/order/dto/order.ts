import { Order as PrismaOrder } from "@prisma/client";

export type Order = PrismaOrder;
